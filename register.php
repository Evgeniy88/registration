<?php
session_start();

if ($_SESSION['user']) {
    header('location: profile.php');
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/main.css">
    <title>Регистрация</title>
</head>
<body>
<!--Форма регистрации-->

<form action="vendor/signup.php" method="POST" enctype="multipart/form-data">
    <label>ФИО</label>
    <input type="text" name="full_name" placeholder="Введите ФИО">
    <label>Логин</label>
    <input type="text" name="login" placeholder="Введите свой логин">
    <label>Изображение профиля</label>
    <input type="file" name="avatar">
    <label>Почта</label>
    <input type="email" name="email" placeholder="Введите адрес почты">
    <label>Пароль</label>
    <input type="password" name="password" placeholder="Введите пароль">
    <label>Подтверидте пароль</label>
    <input type="password" name="password_confirm" placeholder="Подтверидте пароль">
    <button type="submit">Войти</button>
    <p>
        У вас есть аккаунт - <a href="/">Авторизируйтесь</a>!
    </p>
    <?php
    if ($_SESSION['message']) {
        echo '<p class="msg"> ' . $_SESSION['message'] . ' </p>';
    }
    unset($_SESSION['message']);
    ?>
</form>
</body>
</html>